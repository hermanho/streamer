const mutations = {
	setGenres(state, data) {
		state.genres = data || []
	},
	toggleGenre(state, genre) {
	    const check = state.selectedGenres.filter(g => g.id === genre.id);
	    if (check.length) {
	        state.selectedGenres = state.selectedGenres.filter(g => g.id !== genre.id);
		} else {
			state.selectedGenres.push(genre);
		}
	}
};

export default mutations
