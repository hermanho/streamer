const getters = {
	getGenres(state) {
		return state.genres
	},
	getActiveGenres(state) {
		return state.selectedGenres;
	}
}

export default getters
