const actions = {
	setGenres({ commit }, data) {
		commit('setGenres', data);
	},
	toggleGenre({ commit }, genre) {
	    commit('toggleGenre', genre)
	}
}

export default actions
