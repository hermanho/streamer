# streamer-app

> A streaming app

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# development with vue devtools
$ npm run dev

# build for production
$ npm run build

# If any errors on missing modules
`set ELECTRON_BUILDER_ALLOW_UNRESOLVED_DEPENDENCIES=true`

# Build options
https://www.electron.build/configuration/win

For detailed explanation on how things work, checkout [Nuxt.js](https://github.com/nuxt/nuxt.js), [Electron.js](https://electronjs.org/), and [electron-builder](https://www.electron.build/).
